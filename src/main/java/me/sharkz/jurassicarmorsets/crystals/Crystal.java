package me.sharkz.jurassicarmorsets.crystals;

import me.sharkz.jurassicarmorsets.effects.ArmorEffectType;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.Map;

/**
 * @author Roch Blondiaux
 */
public class Crystal {

    private final String name;
    private final ItemStack item;
    private final List<String> lore;
    private final Map<ArmorEffectType, Integer> effects;

    public Crystal(String name, ItemStack item, List<String> lore, Map<ArmorEffectType, Integer> effects) {
        this.name = name;
        this.item = item;
        this.lore = lore;
        this.effects = effects;
    }

    public String getName() {
        return name;
    }

    public ItemStack getItem() {
        return item;
    }

    public List<String> getLore() {
        return lore;
    }

    public Map<ArmorEffectType, Integer> getEffects() {
        return effects;
    }
}
