package me.sharkz.jurassicarmorsets.utils;

import me.sharkz.jurassicarmorsets.crystals.Crystal;
import me.sharkz.jurassicarmorsets.crystals.CrystalManager;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 */
public class CrystalsUtils {

    public static String deserialize(List<Crystal> crystals, Character separator) {
        StringBuilder b = new StringBuilder();
        crystals.forEach(crystal -> b.append(crystal.getName()).append(separator));
        return b.toString();
    }

    public static List<Crystal> serialize(CrystalManager manager, String data, String separator) {
        return Arrays.stream(data.split(separator))
                .map(manager::getCrystalByName)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }
}
