package me.sharkz.jurassicarmorsets.ui;

import com.google.common.collect.ImmutableMap;
import me.sharkz.jurassicarmorsets.AS;
import me.sharkz.jurassicarmorsets.commands.GiveCommand;
import me.sharkz.jurassicarmorsets.sets.SetsManager;
import me.sharkz.jurassicarmorsets.translations.JaEn;
import me.sharkz.milkalib.inventories.InventoryResult;
import me.sharkz.milkalib.inventories.MilkaInventory;
import me.sharkz.milkalib.utils.items.ItemBuilder;
import net.md_5.bungee.api.ChatColor;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Roch Blondiaux
 */
public class MainUI extends MilkaInventory {

    private final SetsManager manager;

    public MainUI() {
        this.manager = AS.getPlugin(AS.class).getSetsManager();
        createInventory(org.bukkit.ChatColor.DARK_GRAY + "Sets", getNearestMultiple(manager.getSets().size() + 1, 9));
    }

    @Override
    public InventoryResult open(Player player, Object... objects) {

        AtomicInteger slot = new AtomicInteger(0);
        manager.getSets()
                .forEach(set -> addItem(slot.getAndIncrement(), new ItemBuilder(Material.BOOK)
                        .setName(ChatColor.WHITE + StringUtils.capitalize(set.getName()))
                        .setLore("&r", "&bLeft click&7 to preview.", "&bMiddle click&7 to get this set.", "&bRight click&7 to get upgrade item."))
                        .setRightClick(e -> {
                            GiveCommand.give(player, manager.getUpgrade(set, 100));

                            message(player, JaEn.UPGRADE_RECEIVED.name(), JaEn.UPGRADE_RECEIVED.toString(), ImmutableMap.of("%set%", set.getName()));
                        })
                        .setMiddleClick(e -> {
                            manager.getArmorSetPieces(set)
                                    .forEach(armorPiece -> GiveCommand.give(player, armorPiece));

                            message(player, JaEn.SET_RECEIVED.name(), JaEn.SET_RECEIVED.toString(), ImmutableMap.of("%set%", set.getName()));
                        })
                        .setLeftClick(e -> openInventory(AS.getPlugin(AS.class), player, 2, set)));

        return InventoryResult.SUCCESS;
    }

}
