package me.sharkz.jurassicarmorsets.loaders;

import me.sharkz.jurassicarmorsets.effects.ArmorEffectType;
import me.sharkz.jurassicarmorsets.crystals.Crystal;
import me.sharkz.milkalib.loaders.ItemStackLoader;
import me.sharkz.milkalib.loaders.MilkaLoader;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;

import java.util.*;

/**
 * @author Roch Blondiaux
 */
public class CrystalLoader implements MilkaLoader<Crystal> {

    private static final ItemStackLoader itemStackLoader = new ItemStackLoader();

    @Override
    public Crystal load(ConfigurationSection sec) {
        if (sec == null) return null;
        ItemStack item = itemStackLoader.load(sec);

        /* Lore */
        List<String> lore = new ArrayList<>();
        if (sec.isList("upgraded-lore")) lore.addAll(sec.getStringList("upgraded-lore"));
        else if (sec.isString("upgraded-lore")) lore.add(sec.getString("upgraded-lore"));

        /* Effects */
        Map<ArmorEffectType, Integer> effects = new HashMap<>();
        ConfigurationSection effectSection = sec.getConfigurationSection("effects");
        if (effectSection != null)
            effectSection.getKeys(true)
                    .forEach(s -> Arrays.stream(ArmorEffectType.values())
                            .filter(armorEffectType -> armorEffectType.name().equalsIgnoreCase(s))
                            .findFirst()
                            .ifPresent(armorEffect -> effects.put(armorEffect, effectSection.getInt(s))));

        return new Crystal(sec.getName(), item, lore, effects);
    }

    @Override
    public void save(Crystal crystalSettings, ConfigurationSection configurationSection) {

    }
}
