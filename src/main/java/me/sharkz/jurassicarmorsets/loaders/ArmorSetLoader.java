package me.sharkz.jurassicarmorsets.loaders;

import me.sharkz.jurassicarmorsets.effects.ArmorEffectType;
import me.sharkz.jurassicarmorsets.sets.ArmorPiece;
import me.sharkz.jurassicarmorsets.sets.ArmorSet;
import me.sharkz.jurassicarmorsets.sets.HeroicSettings;
import me.sharkz.jurassicarmorsets.utils.ItemSet;
import me.sharkz.milkalib.loaders.ItemStackLoader;
import me.sharkz.milkalib.loaders.MilkaLoader;
import me.sharkz.milkalib.utils.logger.MilkaLogger;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;

import java.util.*;

/**
 * @author Roch Blondiaux
 */
public class ArmorSetLoader implements MilkaLoader<ArmorSet> {

    private static final ItemStackLoader itemStackLoader = new ItemStackLoader();
    private static final ArmorPieceLoader armorPieceLoader = new ArmorPieceLoader();
    private static final HeroicLoader heroicLoader = new HeroicLoader();

    private static final ItemSet[] piecesToLoad = new ItemSet[]{ItemSet.WEAPONS, ItemSet.BOOTS, ItemSet.LEGGINGS, ItemSet.CHESTPLATE, ItemSet.HELMET};

    @Override
    public ArmorSet load(ConfigurationSection sec) {
        if (sec == null) return null;
        String name = sec.getString("name", sec.getName());

        String applyMessage = sec.getString("apply-message");
        String removeMessage = sec.getString("remove-message");

        Sound applySound = null;
        Sound removeSound = null;
        if (sec.contains("sounds.apply"))
            applySound = Sound.valueOf(sec.getString("sounds.apply").toUpperCase());
        if (sec.contains("sounds.remove"))
            removeSound = Sound.valueOf(sec.getString("sounds.remove").toUpperCase());

        ItemStack upgradeItem = itemStackLoader.load(sec.getConfigurationSection("upgrade"));

        int requiredItems = sec.getInt("required-items");

        /* Effects */
        Map<ArmorEffectType, Integer> effects = new HashMap<>();
        ConfigurationSection effectSection = sec.getConfigurationSection("effects");
        if (effectSection != null)
            effectSection.getKeys(true)
                    .forEach(s -> Arrays.stream(ArmorEffectType.values())
                            .filter(armorEffectType -> armorEffectType.name().equalsIgnoreCase(s))
                            .findFirst()
                            .ifPresent(armorEffect -> effects.put(armorEffect, effectSection.getInt(s))));

        /* Pieces */
        List<ArmorPiece> pieces = new ArrayList<>();
        for (ItemSet itemSet : piecesToLoad) {
            ArmorPiece piece = armorPieceLoader.load(sec.getConfigurationSection(itemSet.name().toLowerCase()));
            if (piece == null) {
                MilkaLogger.warn("Invalid armor piece in " + name + " config file!");
                continue;
            }
            pieces.add(piece);
        }

        /* Heroic */
        HeroicSettings heroicSettings = heroicLoader.load(sec.getConfigurationSection("heroic"));

        return new ArmorSet(name, applyMessage, removeMessage, applySound, removeSound, requiredItems, upgradeItem, effects, pieces, heroicSettings);
    }

    @Override
    public void save(ArmorSet set, ConfigurationSection configurationSection) {
    }
}
