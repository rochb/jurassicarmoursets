package me.sharkz.jurassicarmorsets.utils;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * @author Roch Blondiaux ( Kiwix | Sharkz )
 */
public enum ItemSet {
    HELMET(5, Material.LEATHER_HELMET, Material.IRON_HELMET, Material.CHAINMAIL_HELMET, Material.GOLD_HELMET, Material.DIAMOND_HELMET),
    CHESTPLATE(6, Material.LEATHER_CHESTPLATE, Material.IRON_CHESTPLATE, Material.CHAINMAIL_CHESTPLATE, Material.GOLD_CHESTPLATE, Material.DIAMOND_CHESTPLATE),
    LEGGINGS(7, Material.LEATHER_LEGGINGS, Material.IRON_LEGGINGS, Material.CHAINMAIL_LEGGINGS, Material.GOLD_LEGGINGS, Material.DIAMOND_LEGGINGS),
    BOOTS(8, Material.LEATHER_BOOTS, Material.IRON_BOOTS, Material.CHAINMAIL_BOOTS, Material.GOLD_BOOTS, Material.DIAMOND_BOOTS),
    SWORD(Material.WOOD_SWORD, Material.IRON_SWORD, Material.STONE_SWORD, Material.GOLD_SWORD, Material.DIAMOND_SWORD),
    BOW(Material.BOW),
    PICKAXE(Material.WOOD_PICKAXE, Material.IRON_PICKAXE, Material.STONE_PICKAXE, Material.GOLD_PICKAXE, Material.DIAMOND_PICKAXE),
    SPADE(Material.WOOD_SPADE, Material.IRON_SPADE, Material.STONE_SPADE, Material.GOLD_SPADE, Material.DIAMOND_SPADE),
    AXE(Material.WOOD_AXE, Material.IRON_AXE, Material.STONE_AXE, Material.GOLD_AXE, Material.DIAMOND_AXE),
    HOE(Material.WOOD_HOE, Material.IRON_HOE, Material.STONE_HOE, Material.GOLD_HOE, Material.DIAMOND_HOE),
    TOOLS(ItemSet.PICKAXE, ItemSet.SPADE, ItemSet.AXE, ItemSet.HOE),
    ARMOURS(ItemSet.HELMET, ItemSet.CHESTPLATE, ItemSet.LEGGINGS, ItemSet.BOOTS),
    WEAPONS(ItemSet.SWORD, ItemSet.AXE);

    private int slot;
    private List<Material> items = new ArrayList<>();

    ItemSet(int slot, Material... items) {
        this.slot = slot;
        this.items.addAll(Arrays.asList(items));
    }

    ItemSet(Material... items) {
        this.slot = 0;
        this.items.addAll(Arrays.asList(items));
    }

    ItemSet(ItemSet... it) {
        for (ItemSet i : it)
            items.addAll(i.getItems());
    }

    public int getSlot() {
        return slot;
    }

    public boolean contains(Material material) {
        return items.contains(material);
    }

    public boolean contains(ItemStack itemStack) {
        return items.contains(itemStack.getType());
    }

    public static Optional<ItemSet> matchType(ItemStack itemStack) {
        if (itemStack == null) return Optional.empty();
        return Arrays.stream(ItemSet.values()).filter(itemSet -> itemSet.contains(itemStack.getType())).findFirst();
    }

    public List<Material> getItems() {
        return items;
    }
}