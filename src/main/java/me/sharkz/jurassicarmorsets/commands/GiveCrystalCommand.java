package me.sharkz.jurassicarmorsets.commands;

import com.google.common.collect.ImmutableMap;
import me.sharkz.jurassicarmorsets.crystals.Crystal;
import me.sharkz.jurassicarmorsets.crystals.CrystalManager;
import me.sharkz.jurassicarmorsets.sets.SetsManager;
import me.sharkz.jurassicarmorsets.translations.JaEn;
import me.sharkz.jurassicarmorsets.utils.CrystalsUtils;
import me.sharkz.milkalib.MilkaPlugin;
import me.sharkz.milkalib.commands.CommandResult;
import me.sharkz.milkalib.commands.MilkaCommand;
import org.apache.commons.lang.StringUtils;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 */
public class GiveCrystalCommand extends MilkaCommand {

    private final CrystalManager crystalManager;

    public GiveCrystalCommand(CrystalManager crystalManager) {
        this.crystalManager = crystalManager;

        setDescription("Give crystal upgrade to players.");
        setPermission("jam.give");

        addSubCommand("givecrystal");

        addRequiredArg("player");
        addRequiredArg("crystal");
        addOptionalArg("chance");

        enableDebugMode(true);
    }

    @Override
    public CommandResult perform(MilkaPlugin milkaPlugin, String[] strings) {
        Player target = argAsPlayer(0);
        if (target == null || !target.isOnline()) {
            message(sender, JaEn.UNKNOWN_PLAYER.name(), JaEn.UNKNOWN_PLAYER.toString());
            return CommandResult.COMPLETED;
        }
        if (argAsString(1).contains(",")) {
            List<Crystal> crystals = new ArrayList<>(CrystalsUtils.serialize(crystalManager, argAsString(1), ","));
            if (crystals.size() == 0) {
                message(sender, JaEn.UNKNOWN_CRYSTAL_UPGRADE.name(), JaEn.UNKNOWN_CRYSTAL_UPGRADE.toString());
                return CommandResult.COMPLETED;
            }
            int chance = 100;
            if (args.length == 3
                    && StringUtils.isNumeric(argAsString(2))
                    && argAsInteger(2) > 0
                    && argAsInteger(2) <= 100)
                chance = argAsInteger(2);

            player.getInventory().addItem(crystalManager.getCrystalUpgrade(chance, crystals));

            StringBuilder c = new StringBuilder();
            crystals.forEach(crystal -> c.append(crystal.getName()).append(", "));
            message(sender, JaEn.CRYSTAL_GAVE.name(), JaEn.CRYSTAL_GAVE.toString(), ImmutableMap.of("%player%", target.getName(), "%crystal%", c.toString(), "%chance%", String.valueOf(chance)));
            return CommandResult.COMPLETED;
        }
        Optional<Crystal> cs = crystalManager.getCrystalByName(argAsString(1));
        if (!cs.isPresent()) {
            message(sender, JaEn.UNKNOWN_CRYSTAL_UPGRADE.name(), JaEn.UNKNOWN_CRYSTAL_UPGRADE.toString());
            return CommandResult.COMPLETED;
        }

        int chance = 100;
        if (args.length == 3
                && StringUtils.isNumeric(argAsString(2))
                && argAsInteger(2) > 0
                && argAsInteger(2) <= 100)
            chance = argAsInteger(2);

        player.getInventory().addItem(crystalManager.getCrystalUpgrade(chance, cs.get()));

        message(sender, JaEn.CRYSTAL_GAVE.name(), JaEn.CRYSTAL_GAVE.toString(), ImmutableMap.of("%player%", target.getName(), "%crystal%", cs.get().getName(), "%chance%", String.valueOf(chance)));
        return CommandResult.COMPLETED;
    }
}
