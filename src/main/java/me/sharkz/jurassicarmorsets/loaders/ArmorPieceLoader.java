package me.sharkz.jurassicarmorsets.loaders;

import me.sharkz.jurassicarmorsets.AS;
import me.sharkz.jurassicarmorsets.effects.ArmorEffectType;
import me.sharkz.jurassicarmorsets.sets.ArmorPiece;
import me.sharkz.jurassicarmorsets.utils.ItemSet;
import me.sharkz.milkalib.loaders.ItemStackLoader;
import me.sharkz.milkalib.loaders.MilkaLoader;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Roch Blondiaux
 */
public class ArmorPieceLoader implements MilkaLoader<ArmorPiece> {

    private static final ItemStackLoader itemStackLoader = new ItemStackLoader();

    @Override
    public ArmorPiece load(ConfigurationSection sec) {
        if (sec == null) return null;
        ItemSet type = ItemSet.valueOf(sec.getName().toUpperCase());
        ItemStack itemStack = itemStackLoader.load(sec);

        Map<ArmorEffectType, Integer> effects = new HashMap<>();
        ConfigurationSection effectSection = sec.getConfigurationSection("effects");
        if (effectSection != null)
            effectSection.getKeys(true)
                    .forEach(s -> Arrays.stream(ArmorEffectType.values())
                            .filter(armorEffectType -> armorEffectType.name().equalsIgnoreCase(s))
                            .findFirst()
                            .ifPresent(armorEffect -> effects.put(armorEffect, effectSection.getInt(s))));

        if (itemStack == null) return null;
        return new ArmorPiece(type, itemStack, effects);
    }

    @Override
    public void save(ArmorPiece armorPiece, ConfigurationSection configurationSection) {

    }
}
