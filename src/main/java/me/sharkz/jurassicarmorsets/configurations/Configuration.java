package me.sharkz.jurassicarmorsets.configurations;

import me.sharkz.jurassicarmorsets.AS;
import me.sharkz.milkalib.configuration.Configurable;
import me.sharkz.milkalib.configuration.wrapper.ConfigurableFileWrapper;
import org.bukkit.configuration.ConfigurationSection;

import java.io.File;

/**
 * @author Roch Blondiaux
 */
public class Configuration extends ConfigurableFileWrapper {


    @Configurable(key = "crystal-upgrades")
    private ConfigurationSection crystals;

    @Configurable(key = "crystal-extractor")
    private ConfigurationSection crystalExtractor;

    public Configuration(AS plugin) {
        super(new File(plugin.getDataFolder(), "config.yml"), plugin.getClass().getClassLoader().getResource("config.yml"));
    }

    public ConfigurationSection getCrystalExtractor() {
        return crystalExtractor;
    }

    public ConfigurationSection getCrystals() {
        return crystals;
    }
}
