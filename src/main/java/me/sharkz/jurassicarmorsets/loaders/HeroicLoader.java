package me.sharkz.jurassicarmorsets.loaders;

import me.sharkz.jurassicarmorsets.effects.ArmorEffectType;
import me.sharkz.jurassicarmorsets.sets.HeroicSettings;
import me.sharkz.milkalib.loaders.MilkaLoader;
import me.sharkz.milkalib.utils.MilkaUtils;
import org.bukkit.Color;
import org.bukkit.configuration.ConfigurationSection;

import java.util.*;

/**
 * @author Roch Blondiaux
 */
public class HeroicLoader extends MilkaUtils implements MilkaLoader<HeroicSettings> {

    @Override
    public HeroicSettings load(ConfigurationSection sec) {
        if (sec == null) return null;
        Color color = parseColor(sec.getString("color"));
        List<String> lore = new ArrayList<>();
        if (sec.isList("lore")) lore.addAll(sec.getStringList("lore"));
        else if (sec.isString("lore")) lore.add(sec.getString("lore"));

        /* Effects */
        Map<ArmorEffectType, Integer> effects = new HashMap<>();
        ConfigurationSection effectSection = sec.getConfigurationSection("effects");
        if (effectSection != null)
            effectSection.getKeys(true)
                    .forEach(s -> Arrays.stream(ArmorEffectType.values())
                            .filter(armorEffectType -> armorEffectType.name().equalsIgnoreCase(s))
                            .findFirst()
                            .ifPresent(armorEffect -> effects.put(armorEffect, effectSection.getInt(s))));

        return new HeroicSettings(color, sec.getInt("armor-points", 4), lore, effects);
    }

    @Override
    public void save(HeroicSettings heroicSettings, ConfigurationSection configurationSection) {

    }
}
