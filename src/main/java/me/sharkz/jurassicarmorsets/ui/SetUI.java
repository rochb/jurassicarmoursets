package me.sharkz.jurassicarmorsets.ui;

import me.sharkz.jurassicarmorsets.AS;
import me.sharkz.jurassicarmorsets.sets.ArmorSet;
import me.sharkz.milkalib.inventories.InventoryResult;
import me.sharkz.milkalib.inventories.MilkaInventory;
import me.sharkz.milkalib.utils.items.ItemBuilder;
import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Roch Blondiaux
 */
public class SetUI extends MilkaInventory {

    public SetUI() {
        createInventory("&c&lError", 9);
    }

    @Override
    public InventoryResult open(Player player, Object... objects) {
        if (objects == null
                || objects.length == 0
                || !(objects[0] instanceof ArmorSet)) return InventoryResult.ERROR;
        ArmorSet set = (ArmorSet) objects[0];

        createInventory(ChatColor.DARK_GRAY + StringUtils.capitalize(set.getName()), 27);

        /* Pieces */
        AtomicInteger slot = new AtomicInteger(10);
        set.getPieces()
                .forEach(a -> addItem(slot.getAndIncrement(), a.getItem()));

        /* Upgrade */
        addItem(16, set.getUpgradeItem());

        /* Back Item */
        addItem(26, new ItemBuilder("eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvODY0Zjc3OWE4ZTNmZmEyMzExNDNmYTY5Yjk2YjE0ZWUzNWMxNmQ2NjllMTljNzVmZDFhN2RhNGJmMzA2YyJ9fX0=")
                .setName("&f&lBack"))
                .setClick(e -> openInventory(AS.getPlugin(AS.class), player, 1));

        return InventoryResult.SUCCESS;
    }
}
