package me.sharkz.jurassicarmorsets;

import me.sharkz.jurassicarmorsets.commands.MainCommand;
import me.sharkz.jurassicarmorsets.configurations.Configuration;
import me.sharkz.jurassicarmorsets.hooks.AdvancedEnchantmentsHook;
import me.sharkz.jurassicarmorsets.listeners.ArmorListener;
import me.sharkz.jurassicarmorsets.listeners.CrystalsListener;
import me.sharkz.jurassicarmorsets.listeners.SetsListener;
import me.sharkz.jurassicarmorsets.sets.SetsManager;
import me.sharkz.jurassicarmorsets.translations.JaEn;
import me.sharkz.jurassicarmorsets.ui.MainUI;
import me.sharkz.jurassicarmorsets.ui.SetUI;
import me.sharkz.milkalib.MilkaPlugin;
import me.sharkz.milkalib.hooks.PapiHook;
import me.sharkz.milkalib.utils.logger.MilkaLogger;

public final class AS extends MilkaPlugin {

    private Configuration config;
    private SetsManager setsManager;

    @Override
    public void onEnable() {
        /* Initialize */
        init(this);

        /* Header */
        MilkaLogger.info("&fDeveloped by &b&lSharkz");
        MilkaLogger.info("&b&lwww.roch-blondiaux.com");
        MilkaLogger.info(" ");
        MilkaLogger.info("&fVersion &b&l" + getDescription().getVersion());
        MilkaLogger.info(" ");

        /* Data folders */
        createFolders();

        /* Dependencies */
        initDependencies();

        /* Hooks */
        initHooks();
        registerHook(new PapiHook());
        registerHook(new AdvancedEnchantmentsHook());

        /* Configurations */
        initConfigurations();
        config = (Configuration) registerConfig(new Configuration(this));

        /* Translations */
        registerTranslation(JaEn.class);

        /* Sets */
        setsManager = new SetsManager(config, getDataFolder());

        /* Commands */
        initCommands();
        registerCommand("ja", new MainCommand(this, setsManager));

        /* UI */
        initInventories();
        registerInventory(1, new MainUI());
        registerInventory(2, new SetUI());

        /* Listeners */
        registerListener(new ArmorListener());
        registerListener(new SetsListener(setsManager));
        registerListener(new CrystalsListener(setsManager.getCrystalManager()));
    }

    @Override
    public void onDisable() {
        /* Website */
        MilkaLogger.info("&b&lwww.roch-blondiaux.com");
    }

    @Override
    public void onReload() {
        super.onReload();

        /* Sets */
        setsManager.load();
    }

    @Override
    protected boolean isPaid() {
        return true;
    }

    @Override
    protected int getConfigurationVersion() {
        return 1;
    }

    @Override
    public String getPrefix() {
        return "&b&lArmor&9&lSets";
    }

    @Override
    public String getLanguage() {
        return getConfig().getString("language", "en_US");
    }

    public SetsManager getSetsManager() {
        return setsManager;
    }
}
