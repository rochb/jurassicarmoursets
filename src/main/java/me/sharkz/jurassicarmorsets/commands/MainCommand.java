package me.sharkz.jurassicarmorsets.commands;

import me.sharkz.jurassicarmorsets.AS;
import me.sharkz.jurassicarmorsets.sets.SetsManager;
import me.sharkz.milkalib.MilkaPlugin;
import me.sharkz.milkalib.commands.CommandResult;
import me.sharkz.milkalib.commands.MilkaCommand;
import me.sharkz.milkalib.commands.defaults.HelpCommand;
import me.sharkz.milkalib.commands.defaults.ReloadCommand;
import me.sharkz.milkalib.commands.defaults.VersionCommand;

/**
 * @author Roch Blondiaux
 */
public class MainCommand extends MilkaCommand {

    public MainCommand(AS plugin, SetsManager manager) {
        setDescription("Main command of the plugin");

        /* Subs */
        addSubCommand(new AdminCommand());
        addSubCommand(new GiveCommand(manager, manager.getCrystalManager()));
        addSubCommand(new GiveCrystalCommand(manager.getCrystalManager()));

        addSubCommand(new VersionCommand());
        addSubCommand(new HelpCommand(plugin));
        addSubCommand(new ReloadCommand(plugin, "jam.reload"));

        enableDebugMode(true);
    }

    @Override
    public CommandResult perform(MilkaPlugin milkaPlugin, String[] strings) {
        HelpCommand.getInstance().sendHelpMenu(sender, 1);
        return CommandResult.COMPLETED;
    }
}
