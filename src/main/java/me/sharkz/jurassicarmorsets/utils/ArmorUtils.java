package me.sharkz.jurassicarmorsets.utils;

import me.sharkz.jurassicarmorsets.effects.ArmorEffectType;
import me.sharkz.jurassicarmorsets.sets.ArmorSet;
import me.sharkz.jurassicarmorsets.sets.SetsManager;
import me.sharkz.milkalib.utils.logger.MilkaLogger;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Roch Blondiaux
 */
public class ArmorUtils {

    public static int getWornPieces(SetsManager manager, Player player, ArmorSet set) {
        AtomicInteger i = new AtomicInteger(0);
        PlayerInventory inv = player.getInventory();
        manager.getByItemStack(inv.getHelmet())
                .ifPresent(set1 -> {
                    if (set1.equals(set)) i.incrementAndGet();
                });
        manager.getByItemStack(inv.getChestplate())
                .ifPresent(set1 -> {
                    if (set1.equals(set)) i.incrementAndGet();
                });
        manager.getByItemStack(inv.getLeggings())
                .ifPresent(set1 -> {
                    if (set1.equals(set)) i.incrementAndGet();
                });
        manager.getByItemStack(inv.getBoots())
                .ifPresent(set1 -> {
                    if (set1.equals(set)) i.incrementAndGet();
                });
        return i.get();
    }

    public static Material getHeroicPiece(ItemStack itemStack) {
        if (!ItemSet.matchType(itemStack).isPresent()) return itemStack.getType();
        switch (ItemSet.matchType(itemStack).get()) {
            case HELMET:
                return Material.LEATHER_HELMET;
            case CHESTPLATE:
                return Material.LEATHER_CHESTPLATE;
            case LEGGINGS:
                return Material.LEATHER_LEGGINGS;
            case BOOTS:
                return Material.LEATHER_BOOTS;
            default:
                return itemStack.getType();
        }
    }

    public static ItemStack changeArmorPoints(ItemStack itemStack, int points) {
        net.minecraft.server.v1_8_R3.ItemStack stack = CraftItemStack.asNMSCopy(itemStack); // Convert the Bukkit ItemStack to a NMS ItemStack.
        NBTTagCompound compound = (stack.hasTag()) ? stack.getTag() : new NBTTagCompound();
        NBTTagList modifiers = new NBTTagList();

        NBTTagCompound toughness = new NBTTagCompound();
        toughness.set("AttributeName", new NBTTagString("armorToughness"));
        toughness.set("Name", new NBTTagString("armorToughness"));
        toughness.set("Amount", new NBTTagInt(10));
        toughness.set("Operation", new NBTTagInt(0));
        toughness.set("UUIDLeast", new NBTTagInt(894654));
        toughness.set("UUIDMost", new NBTTagInt(2872));
        modifiers.add(toughness);

        NBTTagCompound armor = new NBTTagCompound();
        armor.set("AttributeName", new NBTTagString("generic.armor"));
        armor.set("Name", new NBTTagString("generic.armor"));
        armor.set("Amount", new NBTTagInt(points));
        armor.set("Operation", new NBTTagInt(0));
        armor.set("UUIDLeast", new NBTTagInt(894654));
        armor.set("UUIDMost", new NBTTagInt(2872));

        modifiers.add(armor);
        compound.set("AttributeModifiers", modifiers);
        stack.setTag(compound);

        return CraftItemStack.asBukkitCopy(stack);
    }

}
