package me.sharkz.jurassicarmorsets.commands;

import com.google.common.collect.ImmutableMap;
import me.sharkz.jurassicarmorsets.crystals.CrystalManager;
import me.sharkz.jurassicarmorsets.sets.ArmorSet;
import me.sharkz.jurassicarmorsets.sets.SetsManager;
import me.sharkz.jurassicarmorsets.translations.JaEn;
import me.sharkz.milkalib.MilkaPlugin;
import me.sharkz.milkalib.commands.CommandResult;
import me.sharkz.milkalib.commands.MilkaCommand;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 */
public class GiveCommand extends MilkaCommand {

    private final SetsManager manager;
    private final CrystalManager crystalManager;

    public GiveCommand(SetsManager manager, CrystalManager crystalManager) {
        this.manager = manager;
        this.crystalManager = crystalManager;

        addSubCommand("give");
        setPermission("jam.give");
        setDescription("Give armour sets & upgrades.");

        addRequiredArg("player");
        addRequiredArg("sets");
        addOptionalArg("upgrade Chance");
    }

    @Override
    public CommandResult perform(MilkaPlugin milkaPlugin, String[] strings) {
        Player target = argAsPlayer(0);
        if (target == null || !target.isOnline()) {
            message(sender, JaEn.UNKNOWN_PLAYER.name(), JaEn.UNKNOWN_PLAYER.toString());
            return CommandResult.COMPLETED;
        }
        if (argAsString(1).equalsIgnoreCase("extractor")) {
            player.getInventory().addItem(crystalManager.getExtractor());
            message(sender, JaEn.CRYSTAL_EXTRACTOR_GAVE.name(), JaEn.CRYSTAL_EXTRACTOR_GAVE.toString(), ImmutableMap.of("%player%", target.getName()));
            return CommandResult.COMPLETED;
        }

        Optional<ArmorSet> set = manager.getByName(argAsString(1));
        if (!set.isPresent()) {
            message(sender, JaEn.UNKNOWN_SET.name(), JaEn.UNKNOWN_SET.toString());
            return CommandResult.COMPLETED;
        }

        ArmorSet as = set.get();
        if (args.length == 3) {
            manager.getArmorSetPieces(as)
                    .forEach(armorPiece -> give(target, armorPiece));

            message(sender, JaEn.SET_GAVE.name(), JaEn.SET_GAVE.toString(), ImmutableMap.of("%player%", target.getName(), "%set%", as.getName()));
            message(target, JaEn.SET_RECEIVED.name(), JaEn.SET_RECEIVED.toString(), ImmutableMap.of("%set%", as.getName()));
        } else {
            int chance = 100;
            if (StringUtils.isNumeric(argAsString(2)))
                chance = Integer.parseInt(argAsString(2));
            give(target, manager.getUpgrade(as, chance));

            message(sender, JaEn.UPGRADE_GAVE.name(), JaEn.UPGRADE_GAVE.toString(), ImmutableMap.of("%player%", target.getName(), "%set%", as.getName()));
            message(target, JaEn.UPGRADE_RECEIVED.name(), JaEn.UPGRADE_RECEIVED.toString(), ImmutableMap.of("%set%", as.getName()));
        }

        return CommandResult.COMPLETED;
    }

    public static void give(Player player, ItemStack itemStack) {
        if (Arrays.stream(player.getInventory().getContents()).noneMatch(itemStack1 -> itemStack1 == null || itemStack1.getType().equals(Material.AIR)))
            player.getWorld().dropItemNaturally(player.getLocation().add(0, 1, 0), itemStack);
        else
            player.getInventory().addItem(itemStack);
    }

}
