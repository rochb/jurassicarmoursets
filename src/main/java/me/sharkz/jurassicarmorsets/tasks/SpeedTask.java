package me.sharkz.jurassicarmorsets.tasks;

import me.sharkz.jurassicarmorsets.effects.ArmorEffectType;
import me.sharkz.jurassicarmorsets.sets.SetsManager;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author Roch Blondiaux
 */
public class SpeedTask extends BukkitRunnable {

    private final SetsManager manager;
    private final Map<UUID, Float> flySpeeds = new HashMap<>();
    private final Map<UUID, Float> walkSpeeds = new HashMap<>();

    public SpeedTask(SetsManager manager) {
        this.manager = manager;
    }

    @Override
    public void run() {
        Bukkit.getOnlinePlayers()
                .forEach(player -> manager.getActiveEffects(player)
                        .forEach((armorEffectType, integer) -> {
                            if (!armorEffectType.equals(ArmorEffectType.SPEED)) {
                                if (flySpeeds.containsKey(player.getUniqueId())) {
                                    player.setFlySpeed(flySpeeds.get(player.getUniqueId()));
                                    flySpeeds.remove(player.getUniqueId());
                                }
                                if (walkSpeeds.containsKey(player.getUniqueId())) {
                                    player.setWalkSpeed(walkSpeeds.get(player.getUniqueId()));
                                    walkSpeeds.remove(player.getUniqueId());
                                }
                                return;
                            }
                            flySpeeds.putIfAbsent(player.getUniqueId(), player.getFlySpeed());
                            walkSpeeds.putIfAbsent(player.getUniqueId(), player.getWalkSpeed());
                            player.setWalkSpeed(integer / 10F);
                            player.setFlySpeed(integer / 10F);
                        }));
    }
}
