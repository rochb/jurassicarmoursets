package me.sharkz.jurassicarmorsets.crystals;

import me.sharkz.jurassicarmorsets.configurations.Configuration;
import me.sharkz.jurassicarmorsets.loaders.CrystalLoader;
import me.sharkz.jurassicarmorsets.utils.CrystalsUtils;
import me.sharkz.milkalib.loaders.ItemStackLoader;
import me.sharkz.milkalib.utils.MilkaUtils;
import me.sharkz.milkalib.utils.NBTEditor;
import me.sharkz.milkalib.utils.items.ItemBuilder;
import me.sharkz.milkalib.utils.logger.MilkaLogger;
import n3kas.ae.api.AEAPI;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 */
public class CrystalManager extends MilkaUtils {

    private final Configuration config;

    /* Extractor */
    private ItemStack extractor;

    /* Crystals */
    private final List<Crystal> crystals = new ArrayList<>();

    /* NBT TAGS */
    private final String EXTRACTOR_TAG = "Extractor";
    private final String UPGRADE_TAG = "CUpgrade";
    private final String UPGRADE_CHANCE_TAG = "CUpChance";
    private final String CRYSTAL_ITEM_TAG = "CrystalI";

    public CrystalManager(Configuration config) {
        this.config = config;

    }

    public void load() {
        extractor = new ItemStackLoader().load(config.getCrystalExtractor());

        crystals.clear();
        CrystalLoader crystalLoader = new CrystalLoader();
        crystals.addAll(config.getCrystals().getKeys(true)
                .stream()
                .map(s -> config.getCrystals().getConfigurationSection(s))
                .filter(Objects::nonNull)
                .map(crystalLoader::load)
                .filter(Objects::nonNull)
                .collect(Collectors.toList()));

        MilkaLogger.info(crystals.size() + " crystals loaded!");
    }

    /* Crystal Items */
    public boolean isCrystalItem(ItemStack itemStack) {
        return itemStack != null
                && NBTEditor.contains(itemStack, CRYSTAL_ITEM_TAG)
                && CrystalsUtils.serialize(this, NBTEditor.getString(itemStack, CRYSTAL_ITEM_TAG), ";").size() > 0;
    }

    public List<Crystal> getAppliedCrystal(ItemStack itemStack) {
        if (itemStack == null || !isCrystalItem(itemStack)) return new ArrayList<>();
        return CrystalsUtils.serialize(this, NBTEditor.getString(itemStack, CRYSTAL_ITEM_TAG), ";");
    }

    /* Crystal Upgrades */
    public boolean isCrystalUpgrade(ItemStack itemStack) {
        return itemStack != null && NBTEditor.contains(itemStack, UPGRADE_TAG);
    }

    public Optional<Crystal> getCrystalFromUpgradeItemStack(ItemStack itemStack) {
        if (itemStack == null || !isCrystalUpgrade(itemStack)) return Optional.empty();
        return getCrystalByName(NBTEditor.getString(itemStack, UPGRADE_TAG));
    }

    public List<Crystal> getCrystalsFromUpgrade(ItemStack itemStack) {
        if (itemStack == null
                || !NBTEditor.contains(itemStack, UPGRADE_TAG)
                || !NBTEditor.getString(itemStack, UPGRADE_TAG).contains(";")) return new ArrayList<>();
        return CrystalsUtils.serialize(this, NBTEditor.getString(itemStack, UPGRADE_TAG), ";");
    }

    public int getUpgradeChanceByItemStack(ItemStack itemStack) {
        if (itemStack == null || !isCrystalUpgrade(itemStack)) return 0;
        return NBTEditor.getInt(itemStack, UPGRADE_CHANCE_TAG);
    }

    public ItemStack getCrystalUpgrade(int chance, List<Crystal> crystals) {
        ItemStack i = crystals.get(0).getItem();
        ItemMeta meta = i.getItemMeta();
        if (meta.hasDisplayName())
            meta.setDisplayName(meta.getDisplayName().replace("%chance%", String.valueOf(chance)));
        List<String> lore = new ArrayList<>();
        if (meta.hasLore())
            lore.addAll(meta.getLore().stream().map(s -> s.replace("%chance%", String.valueOf(chance))).collect(Collectors.toList()));
        lore.add(" ");
        lore.add("&6&lMulti-Crystal Bonus:");
        crystals.forEach(crystal -> {
            lore.add(ChatColor.BOLD + crystal.getName());
            lore.addAll(crystal.getLore());
            lore.add(" ");
        });
        meta.setLore(color(lore));
        i.setItemMeta(meta);
        return NBTEditor.set(NBTEditor.set(i, CrystalsUtils.deserialize(crystals, ';'), UPGRADE_TAG), chance, UPGRADE_CHANCE_TAG);
    }

    public ItemStack getCrystalUpgrade(int chance, Crystal crystal) {
        ItemStack i = crystal.getItem();
        ItemMeta meta = i.getItemMeta();
        if (meta.hasDisplayName())
            meta.setDisplayName(meta.getDisplayName().replace("%chance%", String.valueOf(chance)));
        if (meta.hasLore())
            meta.setLore(meta.getLore().stream().map(s -> s.replace("%chance%", String.valueOf(chance))).collect(Collectors.toList()));
        i.setItemMeta(meta);
        return NBTEditor.set(NBTEditor.set(i, crystal.getName(), UPGRADE_TAG), chance, UPGRADE_CHANCE_TAG);
    }

    public Optional<Crystal> getCrystalByName(String name) {
        return crystals.stream().filter(crystal -> crystal.getName().equalsIgnoreCase(name)).findFirst();
    }

    public ItemStack applyCrystalUpgrade(ItemStack itemStack, ItemStack upgradeItem) {
        if (itemStack == null
                || !isCrystalUpgrade(upgradeItem)) return null;
        List<Crystal> crystals = getCrystalsFromUpgrade(upgradeItem);
        if (crystals.size() > 0) {
            /* Lore */
            ItemMeta meta = itemStack.getItemMeta();
            List<String> lore = new ArrayList<>();
            if (meta.hasLore())
                lore.addAll(meta.getLore());
            lore.add(" ");
            lore.add("&6&lMulti-Crystal Bonus:");
            crystals.forEach(crystal -> {
                lore.add(ChatColor.BOLD + crystal.getName());
                lore.addAll(crystal.getLore());
                lore.add(" ");
            });
            meta.setLore(color(lore));
            itemStack.setItemMeta(meta);

            /* Applied Crystals */
            List<Crystal> appliedCrystals = new ArrayList<>();
            if (isCrystalItem(itemStack)) appliedCrystals = getAppliedCrystal(itemStack);
            appliedCrystals.addAll(crystals);

            /* NBT */
            return NBTEditor.set(itemStack, CrystalsUtils.deserialize(appliedCrystals, ';'), CRYSTAL_ITEM_TAG);
        }
        Optional<Crystal> cs = getCrystalFromUpgradeItemStack(upgradeItem);
        if (!cs.isPresent()
                || (isCrystalItem(itemStack) && getAppliedCrystal(itemStack).contains(cs.get()))
                || new Random().nextInt(100) > getUpgradeChanceByItemStack(upgradeItem))
            return null;

        /* Lore */
        ItemMeta meta = itemStack.getItemMeta();
        List<String> lore = new ArrayList<>();
        if (meta.hasLore())
            lore.addAll(meta.getLore());
        lore.addAll(cs.get().getLore());
        meta.setLore(color(lore));

        itemStack.setItemMeta(meta);

        /* Applied Crystals */
        List<Crystal> appliedCrystals = new ArrayList<>();
        if (isCrystalItem(itemStack)) appliedCrystals = getAppliedCrystal(itemStack);
        appliedCrystals.add(cs.get());

        /* NBT */
        return NBTEditor.set(itemStack, CrystalsUtils.deserialize(appliedCrystals, ';'), CRYSTAL_ITEM_TAG);
    }


    /* Extractor */
    public ItemStack extractRandomUpgrade(ItemStack itemStack, ItemStack crystalExtractor) {
        if (itemStack == null
                || crystalExtractor == null
                || !isExtractor(crystalExtractor)
                || !isCrystalItem(itemStack)) return null;
        AtomicReference<ItemStack> i = new AtomicReference<>(new ItemBuilder(itemStack).toItemStack());

        /* Advanced Enchantments */
        AEAPI.getEnchantmentsOnItem(itemStack)
                .forEach((s, integer) -> i.set(AEAPI.applyEnchant(s, integer, i.get())));

        /* Applied Crystals */
        List<Crystal> appliedCrystals = getAppliedCrystal(itemStack);
        Crystal selectedCrystal = appliedCrystals.get(new Random().nextInt(appliedCrystals.size()));
        appliedCrystals.remove(selectedCrystal);

        /* Lore */
        List<String> lore = new ArrayList<>();
        ItemMeta meta = itemStack.getItemMeta();
        if (meta.hasLore())
            lore.addAll(meta.getLore());
        lore.removeIf(s -> s.contains(selectedCrystal.getName()));
        lore.removeAll(selectedCrystal.getLore().stream().map(this::color).collect(Collectors.toList()));

        meta.setLore(lore);
        i.get().setItemMeta(meta);

        return NBTEditor.set(i.get(), CrystalsUtils.deserialize(appliedCrystals, ';'), CRYSTAL_ITEM_TAG);
    }

    public boolean isExtractor(ItemStack itemStack) {
        return itemStack != null && NBTEditor.contains(itemStack, EXTRACTOR_TAG);
    }

    public ItemStack getExtractor() {
        return NBTEditor.set(extractor, 1, EXTRACTOR_TAG);
    }
}
