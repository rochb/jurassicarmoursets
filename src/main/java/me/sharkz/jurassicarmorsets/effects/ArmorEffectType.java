package me.sharkz.jurassicarmorsets.effects;

import org.bukkit.event.Listener;

/**
 * @author Roch Blondiaux
 */
public enum ArmorEffectType implements Listener {
    DAMAGE,
    REDUCTION,
    DURABILITY,
    SPEED;
}
