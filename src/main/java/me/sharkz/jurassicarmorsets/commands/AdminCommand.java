package me.sharkz.jurassicarmorsets.commands;

import me.sharkz.milkalib.MilkaPlugin;
import me.sharkz.milkalib.commands.CommandResult;
import me.sharkz.milkalib.commands.MilkaCommand;

/**
 * @author Roch Blondiaux
 */
public class AdminCommand extends MilkaCommand {

    public AdminCommand() {
        setDescription("Manage armor sets.");
        setPermission("jam.admin");

        addSubCommand("admin");

        setConsoleCanUse(false);
    }

    @Override
    public CommandResult perform(MilkaPlugin milkaPlugin, String[] strings) {
        openInventory(milkaPlugin, player, 1);
        return CommandResult.COMPLETED;
    }
}
