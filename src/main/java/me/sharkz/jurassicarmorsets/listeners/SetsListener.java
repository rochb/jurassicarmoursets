package me.sharkz.jurassicarmorsets.listeners;

import me.sharkz.jurassicarmorsets.crystals.CrystalManager;
import me.sharkz.jurassicarmorsets.effects.ArmorEffectType;
import me.sharkz.jurassicarmorsets.events.ArmorEquipEvent;
import me.sharkz.jurassicarmorsets.sets.SetsManager;
import me.sharkz.milkalib.utils.logger.MilkaLogger;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Random;

/**
 * @author Roch Blondiaux
 */
public class SetsListener implements Listener {

    private final SetsManager manager;

    public SetsListener(SetsManager manager) {
        this.manager = manager;
    }

    @EventHandler
    public void onSetUpgrade(InventoryClickEvent e) {
        if (e.getCursor() == null
                || e.getCursor().getType().equals(Material.AIR)
                || e.getCurrentItem() == null
                || e.getCurrentItem().getType().equals(Material.AIR)
                || !manager.canBeUpgrade(e.getCurrentItem())
                || !manager.isUpgradeItem(e.getCursor())) return;
        ItemStack itemStack = manager.upgrade(e.getCurrentItem(), e.getCursor());
        if (itemStack == null) return;
        if (e.getCursor().getAmount() == 1)
            e.setCurrentItem(null);
        else {
            e.setCurrentItem(e.getCursor());
            e.getCurrentItem().setAmount(e.getCursor().getAmount() - 1);
        }
        e.setCursor(itemStack);

    }

    @EventHandler
    public void onSetEquip(ArmorEquipEvent e) {
        if (e.getNewArmorPiece() == null
                || !manager.isArmorSetPiece(e.getNewArmorPiece())) return;
        manager.applySet(e.getPlayer(), e.getNewArmorPiece());
    }

    @EventHandler
    public void onSetUnEquip(ArmorEquipEvent e) {
        if (e.getOldArmorPiece() == null
                || !manager.isArmorSetPiece(e.getOldArmorPiece())) return;
        manager.removeSet(e.getPlayer(), e.getOldArmorPiece());
    }

    @EventHandler
    public void onSetItemsDamage(PlayerItemDamageEvent e) {
        manager.getActiveEffects(e.getPlayer())
                .forEach((armorEffectType, integer) -> {
                    if (armorEffectType.equals(ArmorEffectType.DURABILITY)
                            && new Random().nextInt(100) <= integer)
                        e.setCancelled(true);
                });
    }

    @EventHandler
    public void onEntityDamageSet(EntityDamageEvent e) {
        if (!(e.getEntity() instanceof Player)) return;
        Player player = (Player) e.getEntity();
        manager.getActiveEffects(player)
                .forEach((armorEffectType, integer) -> {
                    if (armorEffectType.equals(ArmorEffectType.REDUCTION))
                        e.setDamage(e.getDamage() - e.getDamage() * (integer / 100D));
                });
    }

    @EventHandler
    public void onEntityDamageBySet(EntityDamageByEntityEvent e) {
        if (!(e.getDamager() instanceof Player)) return;
        Player player = (Player) e.getDamager();
        if (player.getItemInHand() == null
                || !manager.isArmorSetPiece(player.getItemInHand())) return;
        manager.getActiveEffects(player)
                .forEach((armorEffectType, integer) -> {
                    if (armorEffectType.equals(ArmorEffectType.DAMAGE))
                        e.setDamage(e.getDamage() + e.getDamage() * (integer / 100D));
                });
    }

}
