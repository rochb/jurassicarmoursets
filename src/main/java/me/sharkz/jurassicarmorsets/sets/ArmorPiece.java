package me.sharkz.jurassicarmorsets.sets;

import me.sharkz.jurassicarmorsets.effects.ArmorEffectType;
import me.sharkz.jurassicarmorsets.utils.ItemSet;
import org.bukkit.inventory.ItemStack;

import java.util.Map;

/**
 * @author Roch Blondiaux
 */
public class ArmorPiece {

    private final ItemSet type;
    private final ItemStack item;
    private final Map<ArmorEffectType, Integer> effects;

    public ArmorPiece(ItemSet type, ItemStack item, Map<ArmorEffectType, Integer> effects) {
        this.type = type;
        this.item = item;
        this.effects = effects;
    }

    public ItemSet getType() {
        return type;
    }

    public ItemStack getItem() {
        return item;
    }

    public Map<ArmorEffectType, Integer> getEffects() {
        return effects;
    }
}
