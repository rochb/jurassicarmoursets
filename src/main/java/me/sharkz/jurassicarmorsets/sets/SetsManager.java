package me.sharkz.jurassicarmorsets.sets;

import me.sharkz.jurassicarmorsets.AS;
import me.sharkz.jurassicarmorsets.configurations.Configuration;
import me.sharkz.jurassicarmorsets.crystals.Crystal;
import me.sharkz.jurassicarmorsets.crystals.CrystalManager;
import me.sharkz.jurassicarmorsets.effects.ArmorEffectType;
import me.sharkz.jurassicarmorsets.loaders.ArmorSetLoader;
import me.sharkz.jurassicarmorsets.tasks.SpeedTask;
import me.sharkz.jurassicarmorsets.utils.ArmorUtils;
import me.sharkz.jurassicarmorsets.utils.ItemSet;
import me.sharkz.milkalib.configuration.wrapper.YamlFileLoadException;
import me.sharkz.milkalib.configuration.wrapper.YamlFileWrapper;
import me.sharkz.milkalib.utils.MilkaUtils;
import me.sharkz.milkalib.utils.NBTEditor;
import me.sharkz.milkalib.utils.items.ItemBuilder;
import me.sharkz.milkalib.utils.logger.MilkaLogger;
import n3kas.ae.api.AEAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.io.File;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * @author Roch Blondiaux
 */
public class SetsManager extends MilkaUtils {

    private final List<ArmorSet> sets = new ArrayList<>();
    private final File dataFolder;

    /* Crystals */
    private final CrystalManager crystalManager;

    /* NBT Tags */
    public static final String NBT_UPGRADE_TAG = "ArmorUpgrade";
    public static final String NBT_UPGRADE_SET_TAG = "ArmorUpgradeSet";
    public static final String NBT_ARMOR_TAG = "ArmorSet";

    public static final String NBT_HEROIC_TAG = "Heroic";

    public SetsManager(Configuration config, File dataFolder) {
        this.dataFolder = new File(dataFolder, "sets");
        this.crystalManager = new CrystalManager(config);

        Bukkit.getScheduler().runTaskTimer(AS.getPlugin(AS.class), new SpeedTask(this), 20L, 20L);

        load();
    }

    public void load() {
        if (!dataFolder.exists())
            dataFolder.mkdir();

        sets.clear();
        ArmorSetLoader loader = new ArmorSetLoader();

        Arrays.stream(Objects.requireNonNull(dataFolder.list()))
                .filter(s -> s.endsWith(".yml") && !s.equals("config.yml"))
                .map(s -> new YamlFileWrapper(new File(dataFolder, s)))
                .forEach(yamlFileWrapper -> {
                    try {
                        yamlFileWrapper.load();
                        ArmorSet set = loader.load(yamlFileWrapper.get());
                        if (set != null)
                            sets.add(set);
                    } catch (YamlFileLoadException e) {
                        MilkaLogger.warn("Couldn't load armour sets:");
                        e.printStackTrace();
                    }
                });

        MilkaLogger.info(sets.size() + " armour sets loaded!");

        crystalManager.load();
    }

    public boolean isWearingHeroicSet(Player player) {
        PlayerInventory inv = player.getInventory();
        return sets.stream()
                .anyMatch(set -> ArmorUtils.getWornPieces(this, player, set) >= set.getRequiredItems() / 2
                        && (isHeroic(inv.getHelmet())
                        || isHeroic(inv.getChestplate())
                        || isHeroic(inv.getLeggings())
                        || isHeroic(inv.getBoots())));
    }

    public List<ArmorPiece> getWearingArmorPieces(Player player) {
        List<ArmorPiece> pieces = new ArrayList<>();
        PlayerInventory inv = player.getInventory();
        getByItemStack(inv.getHelmet()).flatMap(set -> set.getPieceByType(ItemSet.HELMET)).ifPresent(pieces::add);
        getByItemStack(inv.getChestplate()).flatMap(set -> set.getPieceByType(ItemSet.CHESTPLATE)).ifPresent(pieces::add);
        getByItemStack(inv.getLeggings()).flatMap(set -> set.getPieceByType(ItemSet.LEGGINGS)).ifPresent(pieces::add);
        getByItemStack(inv.getBoots()).flatMap(set -> set.getPieceByType(ItemSet.BOOTS)).ifPresent(pieces::add);
        return pieces;
    }

    public List<Crystal> getWearingCrystals(Player player) {
        List<Crystal> crystals = new ArrayList<>();
        PlayerInventory inv = player.getInventory();
        crystals.addAll(crystalManager.getAppliedCrystal(inv.getHelmet()));
        crystals.addAll(crystalManager.getAppliedCrystal(inv.getChestplate()));
        crystals.addAll(crystalManager.getAppliedCrystal(inv.getLeggings()));
        crystals.addAll(crystalManager.getAppliedCrystal(inv.getBoots()));
        return crystals;
    }

    public Map<ArmorSet, List<ArmorPiece>> getWearingArmorSetsPieces(Player player) {
        Map<ArmorSet, List<ArmorPiece>> a = new HashMap<>();
        List<ArmorPiece> pieces = getWearingArmorPieces(player);
        sets.stream()
                .filter(set -> set.getPieces().stream().anyMatch(pieces::contains))
                .forEach(set -> {
                    a.putIfAbsent(set, new ArrayList<>());
                    set.getPieces()
                            .stream()
                            .filter(pieces::contains)
                            .findFirst()
                            .ifPresent(a.get(set)::add);
                });

        return a;
    }

    public Map<ArmorEffectType, Integer> getActiveEffects(Player player) {
        /* ArmorSet effects */
        Map<ArmorEffectType, Integer> setEffects = new HashMap<>();
        getWearingArmorSet(player)
                .ifPresent(set -> setEffects.putAll(set.getEffects()));

        /* Pieces effects */
        Map<ArmorEffectType, Integer> piecesEffects = new HashMap<>();
        getWearingArmorPieces(player)
                .forEach(armorPiece -> piecesEffects.putAll(armorPiece.getEffects()));

        /* Heroic & Crystal */
        Map<ArmorEffectType, Integer> crystalEffects = new HashMap<>();
        Map<ArmorEffectType, Integer> heroicEffects = new HashMap<>();
        getWearingCrystals(player).forEach(crystal -> crystalEffects.putAll(crystal.getEffects()));
        getWearingArmorSet(player)
                .ifPresent(set -> {
                    if (isWearingHeroicSet(player)) heroicEffects.putAll(set.getHeroicSettings().getEffects());
                });

        Map<ArmorEffectType, Integer> effects = new HashMap<>(setEffects);
        computeEffects(effects, piecesEffects, crystalEffects, heroicEffects);
        return effects;
    }

    @SafeVarargs
    private final void computeEffects(Map<ArmorEffectType, Integer> source, Map<ArmorEffectType, Integer>... toAdd) {
        Arrays.stream(toAdd)
                .forEach(armorEffectTypeIntegerMap -> armorEffectTypeIntegerMap.forEach((armorEffectType, integer) -> {
                    if (source.containsKey(armorEffectType))
                        source.replace(armorEffectType, source.get(armorEffectType) + integer);
                    else
                        source.put(armorEffectType, integer);
                }));
    }

    public Optional<ArmorSet> getWearingArmorSet(Player player) {
        return sets.stream().filter(set -> ArmorUtils.getWornPieces(this, player, set) >= set.getRequiredItems()).findFirst();
    }

    public void applySet(Player player, ItemStack itemStack) {
        getByItemStack(itemStack)
                .ifPresent(set -> {
                    if (ArmorUtils.getWornPieces(this, player, set) + 1 < set.getRequiredItems()) return;
                    if (set.getApplyMessage() != null)
                        player.sendMessage(color(set.getApplyMessage()));
                    if (set.getApplySound() != null)
                        player.playSound(player.getLocation(), set.getApplySound(), 1f, 1f);
                });
    }

    public void removeSet(Player player, ItemStack itemStack) {
        getByItemStack(itemStack)
                .ifPresent(set -> {
                    if (ArmorUtils.getWornPieces(this, player, set) + 1 < set.getRequiredItems()) return;
                    if (set.getRemoveMessage() != null)
                        player.sendMessage(color(set.getRemoveMessage()));
                    if (set.getRemoveSound() != null)
                        player.playSound(player.getLocation(), set.getRemoveSound(), 1f, 1f);
                });
    }

    public boolean isHeroic(ItemStack itemStack) {
        return itemStack != null && NBTEditor.contains(itemStack, NBT_HEROIC_TAG);
    }

    public boolean canBeUpgrade(ItemStack itemStack) {
        return !isUpgradeItem(itemStack) && isArmorSetPiece(itemStack) && !ArmorUtils.getHeroicPiece(itemStack).equals(itemStack.getType());
    }

    public boolean isUpgradeItem(ItemStack itemStack) {
        return itemStack != null
                && NBTEditor.contains(itemStack, NBT_UPGRADE_TAG);
    }

    public int getUpgradeChance(ItemStack itemStack) {
        if (itemStack == null || !isUpgradeItem(itemStack)) return 0;
        return NBTEditor.getInt(itemStack, NBT_UPGRADE_TAG);
    }

    public Optional<ArmorSet> getUpgradeSet(ItemStack itemStack) {
        if (itemStack == null || !isUpgradeItem(itemStack)) return Optional.empty();
        return getByName(NBTEditor.getString(itemStack, NBT_UPGRADE_SET_TAG));
    }

    public ItemStack getUpgrade(ArmorSet set, int chance) {
        ItemMeta meta = set.getUpgradeItem().getItemMeta();
        ItemBuilder ib = new ItemBuilder(set.getUpgradeItem())
                .setName(meta.getDisplayName().replace("%percent%", String.valueOf(chance)))
                .setLore(meta.getLore().stream().map(s -> s.replace("%percent%", String.valueOf(chance))).collect(Collectors.toList()));
        return NBTEditor.set(NBTEditor.set(ib.toItemStack(), chance, NBT_UPGRADE_TAG), set.getName(), NBT_UPGRADE_SET_TAG);
    }

    public ItemStack upgrade(ItemStack itemStack, ItemStack upgradeItem) {
        Optional<ArmorSet> as = getUpgradeSet(upgradeItem);
        if (!as.isPresent()
                || itemStack == null
                || upgradeItem == null
                || !canBeUpgrade(itemStack)
                || !isUpgradeItem(upgradeItem)
                || new Random().nextInt(100) > getUpgradeChance(upgradeItem)) return null;
        ArmorSet set = as.get();
        Optional<ArmorPiece> ap = set.getPieces().stream().filter(armorPiece -> armorPiece.getType().contains(itemStack.getType())).findFirst();
        if (!ap.isPresent()) return null;
        ItemBuilder ib = new ItemBuilder(ArmorUtils.getHeroicPiece(itemStack));

        ItemMeta meta = itemStack.getItemMeta();

        /* Name & Lore */
        ib.setName(meta.getDisplayName());
        ib.setLore(meta.getLore());
        ib.addLoreLine(set.getHeroicSettings().getLore());

        /* Flags */
        ib.addFlags(meta.getItemFlags().toArray(new ItemFlag[]{}));
        ib.addFlags(ItemFlag.HIDE_ATTRIBUTES);

        /* Vanilla Enchantments */
        ib.addEnchantments(itemStack.getEnchantments());

        AtomicReference<ItemStack> i = new AtomicReference<>(ib.toItemStack());

        /* Color */
        LeatherArmorMeta LMeta = (LeatherArmorMeta) i.get().getItemMeta();
        LMeta.setColor(set.getHeroicSettings().getColor());
        i.get().setItemMeta(LMeta);

        /* Durability */
        i.get().setDurability((short) (i.get().getType().getMaxDurability() - 100));

        /* NBT */
        i.set(NBTEditor.set(i.get(), set.getName(), NBT_HEROIC_TAG));
        i.set(NBTEditor.set(i.get(), set.getName(), NBT_ARMOR_TAG));

        /* Advanced Enchantments */
        AEAPI.getEnchantmentsOnItem(itemStack)
                .forEach((s, integer) -> i.set(AEAPI.applyEnchant(s, integer, i.get())));

        /* Armor Points */
        i.set(ArmorUtils.changeArmorPoints(i.get(), set.getHeroicSettings().getArmorPoints()));

        return i.get();
    }

    public boolean isArmorSetPiece(ItemStack itemStack) {
        return itemStack != null && NBTEditor.contains(itemStack, NBT_ARMOR_TAG);
    }

    public Optional<ArmorSet> getByItemStack(ItemStack itemStack) {
        if (itemStack == null || !isArmorSetPiece(itemStack)) return Optional.empty();
        return getByName(NBTEditor.getString(itemStack, NBT_ARMOR_TAG));
    }

    public List<ItemStack> getArmorSetPieces(ArmorSet set) {
        return set.getPieces()
                .stream()
                .map(armorPiece -> NBTEditor.set(armorPiece.getItem(), set.getName(), NBT_ARMOR_TAG))
                .collect(Collectors.toList());
    }

    public Optional<ArmorSet> getByName(String name) {
        return sets.stream().filter(set -> set.getName().equalsIgnoreCase(name)).findFirst();
    }

    public List<ArmorSet> getSets() {
        return sets;
    }

    public CrystalManager getCrystalManager() {
        return crystalManager;
    }
}
