package me.sharkz.jurassicarmorsets.listeners;

import me.sharkz.jurassicarmorsets.crystals.Crystal;
import me.sharkz.jurassicarmorsets.crystals.CrystalManager;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.Random;

/**
 * @author Roch Blondiaux
 */
public class CrystalsListener implements Listener {

    private final CrystalManager manager;

    public CrystalsListener(CrystalManager manager) {
        this.manager = manager;
    }

    @EventHandler
    public void onCrystalUpgrade(InventoryClickEvent e) {
        if (checkItems(e) || !manager.isCrystalUpgrade(e.getCursor())) return;
        ItemStack i = manager.applyCrystalUpgrade(e.getCurrentItem(), e.getCursor());
        if (!replaceItems(e, i)) useCursor(e);
    }

    @EventHandler
    public void onCrystalExtract(InventoryClickEvent e) {
        if (checkItems(e) || !manager.isExtractor(e.getCursor())) return;
        List<Crystal> og = manager.getAppliedCrystal(e.getCurrentItem());
        ItemStack i = manager.extractRandomUpgrade(e.getCurrentItem(), e.getCursor());
        List<Crystal> af = manager.getAppliedCrystal(i);
        if (!replaceItems(e, i)) {
            useCursor(e);
            return;
        }
        og.stream()
                .filter(crystal -> !af.contains(crystal))
                .findFirst()
                .ifPresent(crystal -> e.getInventory().addItem(manager.getCrystalUpgrade(new Random().nextInt(60), crystal)));
    }

    private boolean replaceItems(InventoryClickEvent e, ItemStack i) {
        if (i == null) return false;
        if (e.getCursor().getAmount() == 1)
            e.setCurrentItem(null);
        else {
            e.setCurrentItem(e.getCursor());
            e.getCurrentItem().setAmount(e.getCursor().getAmount() - 1);
        }
        e.setCursor(i);
        return true;
    }

    private void useCursor(InventoryClickEvent e) {
        if (e.getCursor().getAmount() == 1)
            e.setCursor(null);
        else
            e.getCursor().setAmount(e.getCursor().getAmount() - 1);
    }

    private boolean checkItems(InventoryClickEvent e) {
        return e.getCursor() == null
                || e.getCursor().getType().equals(Material.AIR)
                || e.getCurrentItem() == null
                || e.getCurrentItem().getType().equals(Material.AIR);
    }
}
