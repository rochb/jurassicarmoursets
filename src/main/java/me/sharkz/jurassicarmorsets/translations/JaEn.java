package me.sharkz.jurassicarmorsets.translations;

import me.sharkz.milkalib.translations.ITranslation;
import me.sharkz.milkalib.translations.Translation;
import me.sharkz.milkalib.translations.TranslationsManager;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public enum JaEn implements ITranslation {
    UNKNOWN_PLAYER("&c&l[!] &eCouldn't find any player with this name!"),
    UNKNOWN_SET("&c&l[!] &eCouldn't find any armour set with name!"),
    SET_GAVE("&a&l[!] &7You have gave a &b%set%&7 armor set to &b%player%&7!"),
    SET_RECEIVED("&a&l[!] &7You have received a &b%set%&7 armor set!"),
    UPGRADE_GAVE("&a&l[!] &7You have gave a &b%set%&7 upgrade item to &b%player%&7!"),
    UPGRADE_RECEIVED("&a&l[!] &7You have received a &b%set%&7 upgrade item!"),
    CRYSTAL_GAVE("&a&l[!] &7You have gave a &a%crystal%&7 (&a%chance%&7%) crystal upgrade item to %player%!"),
    CRYSTAL_EXTRACTOR_GAVE("&a&l[!] &7You have gave a crystal extractor item to %player%!"),
    UNKNOWN_CRYSTAL_UPGRADE("&c&l[!] &eCouldn't find any crystal upgrade with this name!"),
    ;

    private final String content;
    private TranslationsManager manager;

    JaEn(String content) {
        this.content = content;
    }

    @Override
    public Locale getLanguage() {
        return new Locale("en", "US");
    }

    @Override
    public Translation get() {
        return new Translation(this, this.name(), this.content);
    }

    @Override
    public List<Translation> translations() {
        return Arrays.stream(values()).map(JaEn::get).collect(Collectors.toList());
    }

    @Override
    public void setManager(TranslationsManager manager) {
        this.manager = manager;
    }

    @Override
    public TranslationsManager getManager() {
        return manager;
    }


    @Override
    public String toString() {
        return this.get().translate();
    }
}

