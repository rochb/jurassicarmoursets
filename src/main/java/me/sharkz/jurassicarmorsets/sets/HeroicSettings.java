package me.sharkz.jurassicarmorsets.sets;

import me.sharkz.jurassicarmorsets.effects.ArmorEffectType;
import org.bukkit.Color;

import java.util.List;
import java.util.Map;

/**
 * @author Roch Blondiaux
 */
public class HeroicSettings {

    private final Color color;
    private final int armorPoints;
    private final List<String> lore;
    private final Map<ArmorEffectType, Integer> effects;

    public HeroicSettings(Color color, int armorPoints, List<String> lore, Map<ArmorEffectType, Integer> effects) {
        this.color = color;
        this.armorPoints = armorPoints;
        this.lore = lore;
        this.effects = effects;
    }

    public int getArmorPoints() {
        return armorPoints;
    }

    public Color getColor() {
        return color;
    }

    public List<String> getLore() {
        return lore;
    }

    public Map<ArmorEffectType, Integer> getEffects() {
        return effects;
    }
}
