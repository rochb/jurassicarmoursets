package me.sharkz.jurassicarmorsets.hooks;

import me.sharkz.milkalib.hooks.MilkaHooker;

/**
 * @author Roch Blondiaux
 */
public class AdvancedEnchantmentsHook extends MilkaHooker {

    @Override
    public String getRequiredPlugin() {
        return "AdvancedEnchantments";
    }

    @Override
    public boolean disablePluginIfNotFound() {
        return false;
    }
}
