package me.sharkz.jurassicarmorsets.sets;

import me.sharkz.jurassicarmorsets.effects.ArmorEffectType;
import me.sharkz.jurassicarmorsets.utils.ItemSet;
import org.bukkit.Sound;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author Roch Blondiaux
 */
public class ArmorSet {

    private final String name;

    /* Message */
    private final String applyMessage;
    private final String removeMessage;

    /* Sounds */
    private final Sound applySound;
    private final Sound removeSound;

    /* Required Items */
    private final int requiredItems;

    /* Upgrade */
    private final ItemStack upgradeItem;

    /* Effects */
    private final Map<ArmorEffectType, Integer> effects;

    /* Armor Pieces */
    private final List<ArmorPiece> pieces;

    /* Heroic */
    private final HeroicSettings heroicSettings;

    public ArmorSet(String name, String applyMessage, String removeMessage, Sound applySound, Sound removeSoound, int requiredItems, ItemStack upgradeItem, Map<ArmorEffectType, Integer> effects, List<ArmorPiece> pieces, HeroicSettings heroicSettings) {
        this.name = name;
        this.applyMessage = applyMessage;
        this.removeMessage = removeMessage;
        this.applySound = applySound;
        this.removeSound = removeSoound;
        this.requiredItems = requiredItems;
        this.upgradeItem = upgradeItem;
        this.effects = effects;
        this.pieces = pieces;
        this.heroicSettings = heroicSettings;
    }

    public HeroicSettings getHeroicSettings() {
        return heroicSettings;
    }

    public String getName() {
        return name;
    }

    public String getApplyMessage() {
        return applyMessage;
    }

    public String getRemoveMessage() {
        return removeMessage;
    }

    public Sound getApplySound() {
        return applySound;
    }

    public Sound getRemoveSound() {
        return removeSound;
    }

    public int getRequiredItems() {
        return requiredItems;
    }

    public ItemStack getUpgradeItem() {
        return upgradeItem;
    }

    public Map<ArmorEffectType, Integer> getEffects() {
        return effects;
    }

    public List<ArmorPiece> getPieces() {
        return pieces;
    }

    public Optional<ArmorPiece> getPieceByType(ItemSet set) {
        return pieces.stream().filter(armorPiece -> armorPiece.getType().equals(set)).findFirst();
    }
}

